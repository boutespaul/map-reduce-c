#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <fcntl.h>

int main(int argc, char** argv) {
	sem_t *sem;
	sem_t *sem2;
	key_t shmkey;
	int shmid;
	int size;
	int *array;
	int index;

	//Récupération de la taille de notre tableau en mémoire partagé
	size = atoi(argv[3]);

	//Génération de la clé avec le path passé en argument
	shmkey = ftok(argv[1], 5);

	shmid = shmget(shmkey, (size + 1) * sizeof (int), IPC_CREAT | 0644);

	if (shmid < 0) {
		perror("shmget error\n");
		exit(1);
	}

	array = (int *) shmat(shmid, 0, 0);

	//On récupère les sémaphores
	sem = sem_open("sem", O_CREAT);
	sem2 = sem_open("sem2", O_CREAT);

	//Récupération de l'index i pour l'opération de map-reduce
	index = atoi(argv[2]);

	//Début de section critique (passe à 0)
	sem_wait(sem);

	printf("Process (%d) \n", index);

	//Opération sur l'élément du tableau
	array[index] *= 4;

	//Réduction : notre accumulateur est placé à la fin du tableau + 1
	array[size + 1] += array[index];

	sem_wait(sem2);

	//On lève la section critique (passe à 1)
	sem_post(sem);

	return 0;
}

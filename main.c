#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <fcntl.h>
#include <wait.h>
#include <unistd.h>


int main (int argc, char **argv){
	int i;
	key_t shmkey;
	int shmid;
	sem_t *sem;
	sem_t *sem2;
	pid_t pid;
	int size = atoi(argv[1]);
	int a = size;
	int *array;
	char *args[5];
	char indexParam[32];
	char sizeParam[32];

	//génération clé pour le segment de mémoire partagé
	shmkey = ftok("/dev/null", 5);

	//Allocation du segment
	shmid = shmget(shmkey, (size + 1) * sizeof (int), 0644 | IPC_CREAT);

	if (shmid < 0) {
		perror ("shmget\n");
		exit (1);
	}
	//On attache array à notre segment
	array = (int *) shmat(shmid, 0, 0);

	//On supprime les semaphores si elles existent (au cas ou)
	sem_unlink("sem");
	sem_unlink("sem2");

	//On initialise la sémaphore
	sem = sem_open("sem", O_CREAT, 0644, 1);
	sem2 = sem_open("sem2", O_CREAT, 0644, size);

	//création des n processus fils
	for (i = 0; i < size; i++){
		array[i] = i;
		pid = fork ();
		if (pid < 0) {
			printf ("Fork error.\n");
		} else if (pid == 0) {
			sprintf(indexParam, "%d", i);
			sprintf(sizeParam, "%d", size);

			//Path pour l'executable
			args[0] = "./operation/operation";

			//Path pour le ftok
			args[1] = "/dev/null";

			//Index de la boucle
			args[2] = indexParam;

			//Taille de notre tableau dans le segment de mémoire partagé
			args[3] = sizeParam;

			args[4] = NULL;

			execvp(*args, args);

			break;
		}
	}


	if (pid != 0){
		//On vérifie si tout est traité
		sem_getvalue(sem2, &a);
		while (a > 0) {
			sem_getvalue(sem2, &a);
		}
		printf ("\nAll children have exited.\n");
		printf("Result of map-reduce : %d \n", array[size + 1]);

		//On détache array de la mémoire partagé
		shmdt(array);

		//On supprime le segment
		shmctl(shmid, IPC_RMID, 0);

		//On suppprime les sémaphores
		sem_unlink("sem");
		sem_unlink("sem2");

		exit(0);
	}



}